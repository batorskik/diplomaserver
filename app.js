
/**
 * Module dependencies
 */

var express = require('express'),
  api = require('./routes/api'),
  http = require('http'),
  path = require('path');

var app = module.exports = express();


/**
 * Configuration
 */

// all environments
app.set('port', process.env.PORT || 3000);
app.use(express.static(path.join(__dirname, 'public')));


// development only
if (app.get('env') === 'development') {
  //app.use(express.errorHandler());
}

// production only
if (app.get('env') === 'production') {
  // TODO
}


/**
 * Routes
 */


// JSON API
app.get('/api/name', api.name);
app.get('/api/errors', api.errors);
app.post('/api/error', api.addError);
app.delete('/api/error/:id', api.deleteError);

app.get('/api/statistic', api.statistic);
app.get('/api/statisticCount', api.statisticCount);

/**
 * Start Server
 */

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
